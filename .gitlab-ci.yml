variables:
  DOCKER_DRIVER: overlay2
  DOCKER_VERSION: 19.03.11

  BUILD_IMAGE_NAME: "${CI_REGISTRY_IMAGE}/branches/${CI_COMMIT_REF_SLUG}:${CI_COMMIT_SHA}-${HELM_VARIANT}"

  ALPINE_VERSION: '3.12'

  KUBECTL_VERSION: 1.15.12
  KUBECTL_CHECKSUM: 316dcb768c0963259f685a4c433150dca3191379a8f2789f39ff50c47b995f702f6f9de89f54a1a260e9ca9fa19dd020ccf7a0a8435e31fe3a7bc5f81b23a643

  HELM2_VERSION: 2.17.0
  HELM2_CHECKSUM: f3bec3c7c55f6a9eb9e6586b8c503f370af92fe987fcbf741f37707606d70296

  HELM3_VERSION: 3.2.4
  HELM3_CHECKSUM: 8eb56cbb7d0da6b73cd8884c6607982d0be8087027b8ded01d6b2759a72e34b1

.docker-image-and-service:
  image: "docker:$DOCKER_VERSION"
  services:
    - "docker:$DOCKER_VERSION-dind"

.helm2:
  variables:
    HELM_VARIANT: helm2
    HELM_VERSION: "$HELM2_VERSION"
    HELM_CHECKSUM: "$HELM2_CHECKSUM"

.helm3:
  variables:
    HELM_VARIANT: helm3
    HELM_VERSION: "$HELM3_VERSION"
    HELM_CHECKSUM: "$HELM3_CHECKSUM"

.helm2to3:
  variables:
    HELM_VARIANT: helm2to3

stages:
  - build
  - test
  - release

.build:
  stage: build
  extends: .docker-image-and-service
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
  script:
    - >-
       docker build
       --build-arg "ALPINE_VERSION=$ALPINE_VERSION"
       --build-arg "HELM_VERSION=$HELM_VERSION"
       --build-arg "HELM_CHECKSUM=$HELM_CHECKSUM"
       --build-arg "KUBECTL_VERSION=$KUBECTL_VERSION"
       --build-arg "KUBECTL_CHECKSUM=$KUBECTL_CHECKSUM"
       --tag "$BUILD_IMAGE_NAME" .
    - docker push "$BUILD_IMAGE_NAME"

build:helm2:
  extends: [.helm2, .build]

build:helm3:
  extends: [.helm3, .build]

build:helm-2to3:
  extends: [.helm2to3, .build]
  script:
    - >-
      docker build
      --build-arg "ALPINE_VERSION=$ALPINE_VERSION"
      --build-arg "HELM2_VERSION=$HELM2_VERSION"
      --build-arg "HELM2_CHECKSUM=$HELM2_CHECKSUM"
      --build-arg "HELM3_VERSION=$HELM3_VERSION"
      --build-arg "HELM3_CHECKSUM=$HELM3_CHECKSUM"
      --build-arg "KUBECTL_VERSION=$KUBECTL_VERSION"
      --build-arg "KUBECTL_CHECKSUM=$KUBECTL_CHECKSUM"
      -f helm2to3.Dockerfile
      --tag "$BUILD_IMAGE_NAME" .
    - docker push "$BUILD_IMAGE_NAME"

test-unit:helm2:
  extends: [.helm2]
  stage: test
  image: "$BUILD_IMAGE_NAME"
  script:
    - kubectl version --client
    - tiller -version
    - helm version --client

test-unit:helm3:
  extends: [.helm3]
  stage: test
  image: "$BUILD_IMAGE_NAME"
  script:
    - kubectl version --client
    - tiller -version && exit 0 || echo "Helm 3 should not have tiller"
    - helm version

test-unit:helm-2to3:
  extends: [.helm2to3]
  stage: test
  image: "$BUILD_IMAGE_NAME"
  script:
    - kubectl version --client
    - tiller -version
    - helm2 version --client
    - helm3 version
    - helm3 plugin list | grep 2to3

.test-integration:
  stage: test
  image: "$BUILD_IMAGE_NAME"
  services:
    - name: registry.gitlab.com/gitlab-org/cluster-integration/test-utils/k3s-gitlab-ci/releases/v0.5.0
      alias: k3s
      command: ["server", "--cluster-secret", "some-secret"]
  before_script:
    - kubectl config set-cluster k3s --server https://k3s:6443 --insecure-skip-tls-verify
    - kubectl config set-credentials default --username=node --password=some-secret
    - kubectl config set-context k3s --cluster=k3s --user=default
    - kubectl config use-context k3s
    - kubectl version

test-integration:helm2:
  extends: [.helm2, .test-integration]
  script:
    - tiller &
    - HELM_HOST="localhost:44134" helm version

test-integration:helm3:
  extends: [.helm3, .test-integration]
  script:
    - helm version

test-integration:helm-2to3:
  extends: [.helm2to3, .test-integration]
  script:
    - tiller &
    - HELM_HOST="localhost:44134" helm2 version
    - helm3 version

.release:
  stage: release
  extends: .docker-image-and-service
  before_script:
    - docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
  script:
    - docker pull "$BUILD_IMAGE_NAME"
    - docker tag "$BUILD_IMAGE_NAME" "$RELEASE_IMAGE_NAME"
    - docker push "$RELEASE_IMAGE_NAME"
  only:
    - master

release:helm2:
  extends: [.helm2, .release]
  variables:
    RELEASE_IMAGE_NAME: "${CI_REGISTRY_IMAGE}/releases/${HELM2_VERSION}-kube-${KUBECTL_VERSION}-alpine-$ALPINE_VERSION"

release:helm3:
  extends: [.helm3, .release]
  variables:
    RELEASE_IMAGE_NAME: "${CI_REGISTRY_IMAGE}/releases/${HELM3_VERSION}-kube-${KUBECTL_VERSION}-alpine-$ALPINE_VERSION"

release:helm-2to3:
  extends: [.helm2to3, .release]
  variables:
    RELEASE_IMAGE_NAME: "${CI_REGISTRY_IMAGE}/releases/helm-2to3-${HELM2_VERSION}-${HELM3_VERSION}-kube-${KUBECTL_VERSION}-alpine-$ALPINE_VERSION"
